# Jupyter Hub Docker

## Description

This is a small setup for a dockerized jupyterhub setup for multiple users as outlined in the blog [post](https://remejy.com/simple-setup-of-jupyterhub-in-docker/) by Remejy.

## Installation

### Start the docker container

```bash
docker run -it -p 8000:8000 --name jupyterhub jupyterhub/jupyterhub bash
```

### Inside docker

We start by settiung up users.

```bash
useradd -m labadmin
useradd -m chemist1
```

and then set passwords for each user

```bash
passwd labadmin
passwd chemist1
```

We need to generate the config for Jupyter Hub. We do this by issuing

```bash
jupyterhub --generate-config
```

which will create `jupyterhub_config.py` that we need to edit to add a couple of lines. Note that you should already be at `/srv/jupyterhub/`, and if this is not the case you should move the file there. Open the file with your favorite editor and move the cursor down a couple lines and type the following ( replacing with your admin username if you changed it)

```python
c.Authenticator.admin_users = set('labadmin')
```

Save and exit and continue installing jupyterlab.

```bash
pip install jupyterlab
```

Once that's done you can launch JupyterHub like this

```bash
jupyterhub
```

## Support

There's no support just use it if you want.

## Contributing

If you feel this can be improved just submit a MR.

## Authors and acknowledgment

This guide comes from the blog post [here](https://remejy.com/simple-setup-of-jupyterhub-in-docker/) and I just put the commands in this repo for convenience. 

## License

MIT
